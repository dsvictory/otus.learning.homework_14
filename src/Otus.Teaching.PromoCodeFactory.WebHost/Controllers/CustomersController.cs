﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Grpc.Net.Client;
using CustomersServer;
using System.Net.Http;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {

        private CustomersGrpc.CustomersGrpcClient GetGrpcClient() {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");

            var client = new CustomersGrpc.CustomersGrpcClient(channel);

            return client;
        }

        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var reply = await GetGrpcClient().GetCustomersAsync(new Empty());

            return Ok(reply);
        }
        
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var reply = await GetGrpcClient().GetCustomerAsync(new GetCustomerReq {
                    Id = id.ToString()
                });

            return Ok(reply);
        }

        [HttpPost]
        public async Task<ActionResult<CustomerResponse>> CreateCustomerAsync(CreateOrEditCustomerRequest request) {

            var grpcInput = new CreateOrEditCustomerReq { 
                LastName = request.LastName,
                FirstName = request.FirstName,
                Email = request.Email
            };
            grpcInput.PreferenceIds.AddRange(request.PreferenceIds.Select(x => x.ToString()));

            var reply = await GetGrpcClient().CreateCustomerAsync(grpcInput);

            return CreatedAtAction(nameof(GetCustomerAsync), new { id = reply.Id }, reply.Id);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request) {
            var grpcInput = new CreateOrEditCustomerReq { 
                LastName = request.LastName,
                FirstName = request.FirstName,
                Email = request.Email
            };
            grpcInput.PreferenceIds.AddRange(request.PreferenceIds.Select(x => x.ToString()));

            var reply = await GetGrpcClient().EditCustomerAsync(new EditCustomerReq {
                Id = id.ToString(),
                Data = grpcInput
            });

            return NoContent();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id) {
            var reply = await GetGrpcClient().DeleteCustomerAsync(new DeleteCustomerReq {
                Id = id.ToString()
            });

            return NoContent();
        }
    }
}