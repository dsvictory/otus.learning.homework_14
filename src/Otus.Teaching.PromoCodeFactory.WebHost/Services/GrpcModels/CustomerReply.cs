﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomersServer {
    public partial class CustomerReply {

        public CustomerReply(Customer customer)
        {
            Id = customer.Id.ToString();
            Email = customer.Email;
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Preferences.AddRange(
                customer.Preferences.Select(x => new PreferenceReply()
                {
                    Id = x.PreferenceId.ToString(),
                    Name = x.Preference.Name
                }).ToList()
            );
        }

    }
}
