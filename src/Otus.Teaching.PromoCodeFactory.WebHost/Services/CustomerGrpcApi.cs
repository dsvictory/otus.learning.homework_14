﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using CustomersServer;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services {
    public class CustomerGrpcApi : CustomersGrpc.CustomersGrpcBase {

        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerGrpcApi(IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<CustomerReply> GetCustomer(GetCustomerReq request, ServerCallContext context) {
            var customer =  await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            var response = new CustomerReply(customer);

            return response;
        }

        public override async Task<GetCustomersReply> GetCustomers(Empty request, ServerCallContext context) {
            var customers =  await _customerRepository.GetAllAsync();

            var response = new GetCustomersReply();
            response.Customers.AddRange(
                customers.Select(x => new CustomerShortReply
                {          
                    Id = x.Id.ToString(),
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }).ToList()
            );

            return response;
        }

        public override async Task<CustomerReply> CreateCustomer(CreateOrEditCustomerReq request, ServerCallContext context) {
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.PreferenceIds.Select(x => Guid.Parse(x)).ToList());

            var c = new Customer();
            c.Id = Guid.NewGuid();
            c.FirstName = request.FirstName;
            c.LastName = request.LastName;
            c.Email = request.Email;
            c.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = c.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            await _customerRepository.AddAsync(c);

            return new CustomerReply(c);
        }

        public override async Task<Empty> EditCustomer(EditCustomerReq request, ServerCallContext context) {
            var c = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.Data.PreferenceIds.Select(x => Guid.Parse(x)).ToList());

            c.FirstName = request.Data.FirstName;
            c.LastName = request.Data.LastName;
            c.Email = request.Data.Email;
            c.Preferences = preferences.Select(x => new CustomerPreference()
            {
                CustomerId = c.Id,
                Preference = x,
                PreferenceId = x.Id
            }).ToList();

            await _customerRepository.UpdateAsync(c);

            return new Empty();
        }

        public override async Task<Empty> DeleteCustomer(DeleteCustomerReq request, ServerCallContext context) {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            await _customerRepository.DeleteAsync(customer);

            return new Empty();
        }
    }
}
