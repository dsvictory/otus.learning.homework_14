﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.RealtimeActions {
    public interface ICustomersClient {

        Task ReceiveRequestedCustomers(IEnumerable<CustomerShortResponse> customers);
        Task ReceiveRequestedCustomer(CustomerResponse customer);
        Task CustomerCreatedNotify(CustomerResponse customer);
        Task CustomerUpdatedNotify(Guid customerId);
        Task CustomerDeletedNotify(Guid customerId);

    }
}
