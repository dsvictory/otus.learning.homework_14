﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.RealtimeActions {
    public class CustomersHub : Hub<ICustomersClient> {

        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersHub(IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public async Task GetCustomers()
        {
            /**
             * Здесь творим дела, как в первоначальном API
             */
            await Clients.Caller.ReceiveRequestedCustomers(new List<CustomerShortResponse>());
        }

        public async Task GetCustomer(Guid id)
        {
            /**
             * Здесь творим дела, как в первоначальном API
             */
            await Clients.Caller.ReceiveRequestedCustomer(new CustomerResponse());
        }

        public async Task CreateCustomer(CreateOrEditCustomerRequest request)
        {
            /**
             * Здесь творим дела, как в первоначальном API
             */
            await Clients.Caller.CustomerCreatedNotify(new CustomerResponse());
        }

        public async Task EditCustomer(Guid id, CreateOrEditCustomerRequest request)
        {
            /**
             * Здесь творим дела, как в первоначальном API
             */
            await Clients.Caller.CustomerUpdatedNotify(id);
        }

        public async Task DeleteCustomer(Guid id)
        {
            /**
             * Здесь творим дела, как в первоначальном API
             */
            await Clients.Caller.CustomerDeletedNotify(id);
        }

    }
}
